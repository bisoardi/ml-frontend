import React from "react";
import '../scss/SearchBar.scss'

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    var url = `/api/items?q=${this.state.value}`
    fetch(url)
      .then(res => { console.log(res); alert(res)} )
      .then(items => this.setState({ items }));
    // alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
    // const axios = require('axios');

    // axios.get(`/api/items?q=${this.state.value}`)
    //     .then(response => {
    //       console.log(response.data)
    //         alert(response.data);
    //     })
    //     .catch(error => {
    //         console.log(error);
    //     });
  }

  render(){
    return(
      <header className="ml-search-bar">
        <div className="search-container">
          <form method="get" onSubmit={this.handleSubmit}>
            <a href="/"></a>
            <input type="text" value={this.state.value} placeholder="Nunca dejes de buscar"
              name="search" onChange={this.handleChange} />
            <button type="submit"></button>
          </form>
        </div>
      </header>
    );
  }
}

export default SearchBar;


