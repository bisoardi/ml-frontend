var request = require('request-promise');

var itemsUrl = 'https://api.mercadolibre.com/sites/MLA/search?q=';
var itemByIdUrl = 'https://api.mercadolibre.com/items/';

var categories = [];

module.exports = {


  fetchItemsData: function(query) {
    return request(itemsUrl+query).then(res => {
      data = JSON.parse(res);

      let items = [];
      

      for (var i = data.results.length - 1; i >= 0; i--) {
        items[i] = {
          "id": data.results[i].id,
          "title": data.results[i].title,
          "price": {
            "currency": data.results[i].currency_id,
            "amount": Math.trunc(data.results[i].price),
            "decimals": (data.results[i].price)-Math.floor(data.results[i].price)
          },
          "picture": data.results[i].thumbnail,
          "condition": data.results[i]. condition,
          "free_shipping": data.results[i].shipping.free_shipping,
          "address": data.results[i].address.state_name
          }
      }

      for (var i = data.filters[0].values[0].path_from_root.length - 1; i >= 0; i--) {
        categories[i] = data.filters[0].values[0].path_from_root[i].name;
      }

      var itemsList = { "categories": categories, "items": items };
      return itemsList;
   })
    .catch(err => {
      console.log(err);
   });
  },

  fetchItemByID : function(id) {
    return request(itemByIdUrl+id).then(res => {
      data = JSON.parse(res);
      let item = {};

      item = {
        "id": id,
        "title": data.title,
        "price": {
          "currency": data.currency_id,
          "amount": Math.trunc(data.price),
          "decimals": (data.price)-Math.floor(data.price)
        },
        "picture": data.pictures[0].url,
        "condition": data.condition === "new" ? "Nuevo" : "Usado",
        "free_shipping": data.shipping.free_shipping,
        "sold_quantity": data.sold_quantity,
        "description": ""
        }

      item = { "categories": categories, "item": item };
      return item;
    })
    .catch(err => {
      console.log(err);
    })
  },

  fetchDescription: function(params) {
    console.log(params);
      return  request(itemByIdUrl+params.id + '/' + params.descriptions)
      .then(res => {
        data = JSON.parse(res);
        return data[0].plain_text
      })
      .catch(err => {
        console.log(err)
      });
  }

}